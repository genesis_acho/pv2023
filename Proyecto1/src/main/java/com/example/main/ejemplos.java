package com.example.main;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ejemplos {
	
	@RequestMapping("/saludo")//peticion sin parametro
	@ResponseBody
	public String saludo() {
		return "Programacion visual";
	}
	
	@RequestMapping("/saludo/{nombre}/{apellido}")//peticion con parametro
	@ResponseBody
	public String saludo(@PathVariable("nombre")String nombre, @PathVariable("apellido")String apellido) {
		return "Bienvenido " + nombre + " " + apellido + " a Spring Boot";
	} 
	

}
